const fs = require('fs');
const path = require('path');
const express = require('express');
const app =  express();
const httpClient = require('http');
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const nconf = require('nconf');
const winston = require('winston');
const osc = require('osc');
const {spawn} = require('child_process');
const glob = require('glob');

/*Config*/
//If config_local doesn't exist then create it by copying config_defaults, then tell nconf to use it:
const localConfigFile = 'config_local.json';
if (!fs.existsSync(localConfigFile)) {
    let defaultConfig = require('./config_defaults.json');
    fs.writeFileSync(localConfigFile, JSON.stringify(defaultConfig, null, 2));
}

nconf.argv().env().file({file: localConfigFile});

/*Logging*/
const logFormat = winston.format.printf(({ level, message, label, timestamp }) => {
    return `${timestamp} [${label}] ${level}: ${message}`;
});

const launchTimeString = new Date().toISOString().replace('T', '_').replace(/[Z\-:\.]/g, '').slice(0, -3);
const logger = winston.createLogger({
    level: 'info',
    format: winston.format.combine(
        winston.format.label({label: 'MiddleBit'}),
        winston.format.timestamp(),
        logFormat
    ),
    transports: [
        new winston.transports.Console({
            format: winston.format.combine(
                winston.format.colorize(),
                winston.format.label({label: 'MiddleBit'}),
                winston.format.timestamp(),
                logFormat
            ),
        }),
        new winston.transports.File({ filename: __dirname + '/logs/MiddleBit_Combined_' + launchTimeString + '.log' , level: 'info'})
    ],
    exceptionHandlers: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: __dirname + '/logs/MiddleBit_Exceptions_' + launchTimeString + '.log' })
    ]
});
logger.info('MiddleBit started.');

/* YOLO OSC */
const yoloParameters = nconf.get('YOLO:params');
const yoloRemoteIP = nconf.get('YOLO:remoteIPAddress');
const yoloRemoteOSCPort = nconf.get('YOLO:remoteOSCPort');
const yoloOSCPort = new osc.UDPPort({localAddress : '0.0.0.0', localPort : nconf.get('YOLO:oscListenPort')});

yoloOSCPort.on('ready', function() {
    logger.info('YOLO: OSC listening on UDP ' + nconf.get('YOLO:oscListenPort') + '. Sending to ' + yoloRemoteIP + ':' + yoloRemoteOSCPort);
});

yoloOSCPort.on('message', function(msg, timestamp, info) {
    if (msg.address === '/yolo/initialised') {
        logger.info("YOLO reports initialised: sending parameters");
        yoloParameters.forEach(function(p){
            //sendParameterToOSCPort(p, yoloOSCPort, yoloRemoteIP, yoloRemoteOSCPort);
        });

        sendParameterToOSCPort({oscAddress : "/yolo/initialised", typeTag: "u", value: true}, yoloOSCPort, yoloRemoteIP, yoloRemoteOSCPort);
    }
});

yoloOSCPort.on('error', function(err) {
    logger.warn("YOLO OSC: " + err);
});

yoloOSCPort.open();

/* Processes */
function spawnProcess(processPath, args, cwd, onSuccess, onFail) {
    logger.info("Spawning process " + processPath + " " + args.join(' '));

    let p = spawn(processPath, args, {cwd : cwd, shell: true});

    p.on("error", function(err) {
        logger.error("Couldn't spawn " + processPath + " with args: " + args.join(' '));
        onFail();
    });

    p.on("exit", function(code, signal) {
        if (code === 0) {
            logger.info( "Process exited with code " + code);
            onSuccess();
        } else {
            logger.warn( "Process exited with code " + code);
            onFail();
        }
    });

    p.stdout.on("data", function(data) {
        logger.info("process stdout:" + data.toString().replace(/\r?\n|\r/g, " "));
    });

    p.stderr.on("data", function(data) {
        logger.info("process stderr:" + data.toString().replace(/\r?\n|\r/g, " "));
    });
}


/* FFMPEG */
const ffmpegPath = nconf.get('ffmpegOutput:path');
const ffmpegOutputSetupArgs = nconf.get('ffmpegOutput:setupArgs');
const ffmpegOutputEncodingArgs = nconf.get('ffmpegOutput:encodingArgs');

function ffmpegImageSequenceToPR4444(inputPath, outputPath, onSuccess, onFail) {
    let ffmpegInputArgs = [];
    ffmpegInputArgs.push("-i");
    ffmpegInputArgs.push(inputPath);

    let ffmpegArgs = ffmpegOutputSetupArgs.concat(ffmpegInputArgs).concat(ffmpegOutputEncodingArgs);
    ffmpegArgs.push(outputPath);
    logger.info("Spawning FFMPEG with args " + ffmpegArgs);

    var ffmpeg = spawn(ffmpegPath, ffmpegArgs);

    ffmpeg.on("error", function(err) {
        logger.error("Error spawning FFMPEG: " + err);
        onFail();
    })

    ffmpeg.on("exit", function(code) {
        if (code === 0) {
            logger.info("FFMPEG complete");
            onSuccess();
        } else {
            logger.error("FFMPEG exited with code " + code);
            onFail();
        }
    });

    ffmpeg.stderr.on('data', function(data){
        logger.info("FFMPEG stderr: " + data.toString().replace(/\r?\n|\r/g, " "));
    })
}

const ffmpegInputSetupArgs = nconf.get('ffmpegInput:setupArgs');
const ffmpegInputEncodingArgs = nconf.get('ffmpegInput:encodingArgs');

function ffmpegVideoToMJPEGSequence(inputPath, outputPath) {
    let ffmpegInputArgs = [];
    ffmpegInputArgs.push("-i");
    ffmpegInputArgs.push(inputPath);

    let ffmpegArgs = ffmpegInputSetupArgs.concat(ffmpegInputArgs).concat(ffmpegInputEncodingArgs);
    ffmpegArgs.push(path.join(outputPath, "input_%7d.jpg"));
    logger.info("Spawning FFMPEG with args " + ffmpegArgs);

    var ffmpeg = spawn(ffmpegPath, ffmpegArgs);

    ffmpeg.on("error", function(err) {
        logger.error("Error spawning FFMPEG: " + err);
    })

    ffmpeg.on("exit", function(code) {
        if (code === 0) {
            logger.info("FFMPEG complete");
            setJobState(activeJob, jobStages.SOURCE_IMAGE_SEQUENCE_TRANSCODED);
            nconf.save();
        } else {
            logger.error("FFMPEG exited with code " + code);
            setJobState(activeJob, jobStages.ERROR);
        }
    });

    ffmpeg.stderr.on('data', function(data){
        logger.info("FFMPEG stderr: " + data.toString().replace(/\r?\n|\r/g, " "));
    })
}

/* Jobs */
const jobStages = {
    NEW : "new",
    TRANSCODING_SOURCE_IMAGE_SEQUENCE : "transcoding-input-image-sequence",
    SOURCE_IMAGE_SEQUENCE_TRANSCODED : "source-image-sequence-transcoded",
    RENDERING_YOLO : "rendering-yolo",
    YOLO_RENDERING_COMPLETE : "yolo-rendering-complete",
    RENDERING_YOLO_PILLOW : "rendering-yolo-pillow",
    YOLO_PILLOW_RENDERING_COMPLETE : "yolo-pillow-rendering-complete",
    TRANSCODING_YOLO_BOXES_MOV : "transcoding-yolo-boxes-mov",
    TRANSCODING_YOLO_BOXES_MOV_COMPLETE : "transcoding-yolo-boxes-mov-complete",
    TRANSCODING_YOLO_LABELS_MOV : "transcoding-yolo-labels-mov",
    TRANSCODING_YOLO_LABELS_MOV_COMPLETE : "transcoding-yolo-labels-mov-complete",
    FINISHED : "complete",
    ERROR : "error"
}

const jobs = nconf.get('jobs:queue');
var activeJob = null;

function restoreActiveJobFromDisc() {
    let j = nconf.get('jobs:activeJobPath');
    if (j) {
        activeJob = jobs.find(jb => jb.inputVideoPath === j);
        logger.info("Loaded job " + activeJob.inputVideoPath + " from disc.");
    }
}
restoreActiveJobFromDisc();

function clearActiveJob() {
    activeJob = null;
    nconf.set('jobs:activeJobPath', null);
    nconf.save();
    logger.info("Cleared active job.");
}

function setJobState(job, state) {
    job.currentState = state;
    logger.info("Changed job " + job.inputVideoPath + " state to " + state);
}

function makeNewJob(inputVideoPath) {
    let job = {};
    job.inputVideoPath = inputVideoPath;

    let clipBaseName = path.basename(inputVideoPath);
    let lastDotIndex = clipBaseName.lastIndexOf('.');
    job.clipName = clipBaseName.slice(0, lastDotIndex);
    job.currentState = jobStages.NEW;
    job.created = new Date().toString();
    job.completed = null;

    jobs.push(job);
    nconf.save();

    logger.info("Created new job:\n" + JSON.stringify(job));
}

function findNewJobs() {
    let inputPath = nconf.get('pipeline:inputDirectory');
    //logger.info("Looking for new videos in " + inputPath);

    let inputVideos = glob.sync('/**/*.mp4', {cwd: inputPath});
    inputVideos.forEach(function(v){
        let rel = path.relative(inputPath, v);
        let matches = jobs.filter(j => j.inputVideoPath === rel);
        if (matches.length === 0) {
            makeNewJob(rel);
        }
    });
}

function createNextLabelImage(inputLabelFiles) {
    if (inputLabelFiles.length === 0) {
        setJobState(activeJob, jobStages.YOLO_PILLOW_RENDERING_COMPLETE);
        nconf.save();
        return;
    }

    let tempPath = nconf.get('pipeline:workingDirectory');
    let inputPath = path.join(tempPath, path.dirname(activeJob.inputVideoPath), activeJob.clipName, "out");

    let l = inputLabelFiles.shift();
    let args = JSON.parse(JSON.stringify(nconf.get('YOLOLabelRenderer:args')));
    args.push('--labelCoordinatesFile');
    args.push('"'+ l + '"');
    let frame = path.basename(l).split('_')[1].split('.')[0];
    args.push('--referenceImageFile')
    args.push('"' + path.join(inputPath, 'boxes_' + frame + '.png') + '"');
    args.push('--outputPath');
    args.push('"' + inputPath + '"');

    logger.info("Spawning labeller with args: " + args.join(" "));

    spawnProcess(nconf.get('YOLOLabelRenderer:path'), args, __dirname,
        function() { //success
            createNextLabelImage(inputLabelFiles);
        },
        function() { //fail
            setJobState(activeJob, jobStages.ERROR);
    });
}

function checkForRequiredActions() {
    let inputPath = nconf.get('pipeline:inputDirectory');
    let tempPath = nconf.get('pipeline:workingDirectory');

    findNewJobs();

    if (activeJob === null) {
        let j = jobs.find(j => j.currentState !== jobStages.FINISHED && j.currentState !== jobStages.ERROR);
        if (j) {
            activeJob = j;
            nconf.set('jobs:activeJobPath', activeJob.inputVideoPath);
            nconf.save();
        } else {
            return;
        }
    }

    if (activeJob.currentState === jobStages.NEW) {
        //Make directory for input image sequence:
        let inputImageSequencePath = path.join(tempPath, path.dirname(activeJob.inputVideoPath), activeJob.clipName, "in");
        fs.mkdirSync(inputImageSequencePath, {recursive: true});
        logger.info("Created " + inputImageSequencePath);

        //Transcode our clip to a JPEG sequence
        ffmpegVideoToMJPEGSequence(path.join(inputPath, activeJob.inputVideoPath), inputImageSequencePath);

        setJobState(activeJob, jobStages.TRANSCODING_SOURCE_IMAGE_SEQUENCE);
    }

    if (activeJob.currentState === jobStages.SOURCE_IMAGE_SEQUENCE_TRANSCODED) {
        let args = JSON.parse(JSON.stringify(nconf.get('YOLO:launchArgs')));

        //Make directory for output image sequence:
        let outputImageSequencePath = path.join(tempPath, path.dirname(activeJob.inputVideoPath), activeJob.clipName, "out");
        fs.mkdirSync(outputImageSequencePath, {recursive: true});
        logger.info("Created " + outputImageSequencePath);

        args.push('"' + path.join(tempPath, path.dirname(activeJob.inputVideoPath), activeJob.clipName, "in", "input_%07d.jpg") + '"');
        args.push('-outDirectory');
        args.push('"' + outputImageSequencePath + '"');

        spawnProcess(nconf.get('YOLO:binaryPath'), args, path.dirname(nconf.get('YOLO:binaryPath')),
            function() { //success
                setJobState(activeJob, jobStages.YOLO_RENDERING_COMPLETE);
                nconf.save();
            },
            function() { //fail
                setJobState(activeJob, jobStages.ERROR);
            });

        setJobState(activeJob, jobStages.RENDERING_YOLO);
    }

    if (activeJob.currentState === jobStages.YOLO_RENDERING_COMPLETE) {
        activeJob.currentState = jobStages.RENDERING_YOLO_PILLOW;

        let inputPath = path.join(tempPath, path.dirname(activeJob.inputVideoPath), activeJob.clipName, "out");
        let inputLabelFiles = glob.sync(path.join(inputPath, '/**/*.labels'), {});

        createNextLabelImage(inputLabelFiles);
    }

    if (activeJob.currentState === jobStages.YOLO_PILLOW_RENDERING_COMPLETE) {
        activeJob.currentState = jobStages.TRANSCODING_YOLO_BOXES_MOV;

        let inputPath = path.join(tempPath, path.dirname(activeJob.inputVideoPath), activeJob.clipName, "out", "boxes_%7d.png");
        let outputPath = path.join(nconf.get('pipeline:outputDirectory'), path.dirname(activeJob.inputVideoPath), activeJob.clipName + " YOLO BOXES.mov");
        fs.mkdirSync(path.dirname(outputPath), {recursive: true});

        ffmpegImageSequenceToPR4444(inputPath, outputPath,
            function() {
                setJobState(activeJob, jobStages.TRANSCODING_YOLO_BOXES_MOV_COMPLETE);
                nconf.save();
            },
            function () {
                setJobState(activeJob, jobStages.ERROR);
            });
    }

    if (activeJob.currentState === jobStages.TRANSCODING_YOLO_BOXES_MOV_COMPLETE) {
        activeJob.currentState = jobStages.TRANSCODING_YOLO_LABELS_MOV;

        let inputPath = path.join(tempPath, path.dirname(activeJob.inputVideoPath), activeJob.clipName, "out", "labels_%7d.png");
        let outputPath = path.join(nconf.get('pipeline:outputDirectory'), path.dirname(activeJob.inputVideoPath), activeJob.clipName + " YOLO LABELS.mov");
        ffmpegImageSequenceToPR4444(inputPath, outputPath,
            function() {
                setJobState(activeJob, jobStages.TRANSCODING_YOLO_LABELS_MOV_COMPLETE);
                activeJob = null;

                nconf.save();
            },
            function () {
                setJobState(activeJob, jobStages.ERROR);
            });
    }

    if (activeJob.currentState === jobStages.TRANSCODING_YOLO_LABELS_MOV_COMPLETE) {
        logger.info("Job complete for " + activeJob.inputVideoPath + ". Deleting temp files");

        let dir = path.join(tempPath, path.dirname(activeJob.inputVideoPath));

        try {
            fs.rmdirSync(dir, {recursive: true});
            setJobState(activeJob, jobStages.FINISHED);
            nconf.save();
        } catch (ex) {
            logger.error("Couldn't delete temp directory " + dir + " :" + ex);
            setJobState(activeJob, jobStages.ERROR);
            nconf.save();
        }

        clearActiveJob();
    }


    if (activeJob && activeJob.currentState === jobStages.ERROR) {
        logger.error("Unloading job " + activeJob.inputVideoPath + " because it is in error state.");
        clearActiveJob();
    }

    io.emit('/currentJob', activeJob);
    io.emit('/jobs', jobs)
}

setInterval(checkForRequiredActions, 1000);

/*Webserver*/
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use('/styles.css', express.static(__dirname + '/styles.css'));

/*WS*/
io.on('connection', function (socket) {
    logger.info("New socketio connection from " + socket.handshake.address);

    socket.emit('/yoloParameters', yoloParameters);

    socket.on('/osc', function(data) {
        if (data.oscAddress.split('/')[1] === 'yolo') {
            let p = yoloParameters.find(p => p.oscAddress === data.oscAddress);
            if (p == null) {
                logger.error("Couldn't find parameter with address " + data.oscAddress);
            } else {
                if (p.typeTag === 'u') { //use u as a tag for finding bools, which we just invert
                    p.value = !p.value;
                } else {
                    p.value = data.value;
                }

                sendParameterToOSCPort(p, yoloOSCPort, yoloRemoteIP, yoloRemoteOSCPort);
            }
        }
    });
});

http.listen(nconf.get('server:httpListenPort'), function () {
    logger.info('listening on *:' + nconf.get('server:httpListenPort'));
});

function sendParameterToOSCPort(parameter, oscPort, remoteIP, remotePort) {
    if (parameter.typeTag === 'f' || parameter.typeTag === 'i') {
        oscPort.send({address: parameter.oscAddress, args: [{type : parameter.typeTag, value: parameter.value}]}, remoteIP, remotePort);
    } else if (parameter.typeTag === 'u') { //use the u type tag as hack for identifying bools
        oscPort.send({address: parameter.oscAddress, args: [{type : 'i', value: parameter.value}]}, remoteIP, remotePort);
    }

    logger.info("Sending " + parameter.value + " to " + parameter.oscAddress);
    io.emit('/osc', parameter);
}

