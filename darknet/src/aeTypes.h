#pragma once
#include <ctype.h>

typedef struct {
    bool initialised;
    bool enableBoxes;
    bool enableClassText;

    bool writeOutputFiles;

    bool overrideBoxColor;
    float boxColorRed;
    float boxColorBlue;
    float boxColorGreen;

    bool overrideTextColor;
    float textColorRed;
    float textColorGreen;
    float textColorBlue;

    bool overrideBoxStroke;
    int boxStroke;

    bool hideLabelBackground;

    bool overrideLabelBackgroundColor;
    float labelBackgroundColorRed;
    float labelBackgroundColorGreen;
    float labelBackgroundColorBlue;

    bool overrideLabelFontSize;
    float labelFontSize;


} ae_parameters_t;
