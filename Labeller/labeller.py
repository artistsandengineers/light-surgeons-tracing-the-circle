import os
import argparse
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

parser = argparse.ArgumentParser(description='Make an image from an input labels file.')
parser.add_argument('--labelCoordinatesFile', nargs=1, required=True, help='The file containing a list of labels.')
parser.add_argument('--referenceImageFile', nargs=1, required=True, help='An image to use as a size and name reference.')
parser.add_argument('--outputPath', nargs=1, required=True, help="The directory to which we'll write our output image.")

args = vars(parser.parse_args())

fontPath = os.path.join(os.path.dirname(os.path.abspath(__file__)), "fonts", "Menlo-Regular-01.ttf")
print("Loading font from ", fontPath)
font = ImageFont.truetype(fontPath, 11)

refImgPath = args['referenceImageFile'][0]
refImg = Image.open(refImgPath)
refImgSize = refImg.size
print("Reference image resolution ", refImgSize[0], "x", refImgSize[1])

labelsImage = Image.new('RGBA', refImgSize)

draw = ImageDraw.Draw(labelsImage)

labelsFile = open(args['labelCoordinatesFile'][0], 'r')
labels = labelsFile.readlines()

#labelsImage.paste(refImg)

for label in labels:
    bits = label.split(',')
    classLabel = bits[0]
    p1x = int(bits[1])
    p1y = int(bits[2])
    p2x = int(bits[3])
    p2y = int(bits[4])

    draw.text((p1x, p1y - 15), classLabel ,(255,255,255,255),font=font)

frameNumber = os.path.basename(refImgPath).split('_')[1] #gives us the frame number and the png extension
outputFileName = os.path.join(args['outputPath'][0], 'labels_' + frameNumber)

labelsImage.save(outputFileName)
print("Wrote ", outputFileName)