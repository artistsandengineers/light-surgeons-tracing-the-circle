# Wat

ML Pipeline tools for Light Surgeons tracing the circle.

# YOLO

## Building

* Clone this repo to `c:\` (anywhere else and the path lengths end up too long...)
* Install VS2017
* Intall CMake GUI 3.20 (the build tools will complain and auto update this to 3.27, but if you've got anything newer than 3.27 installed nothing will work.)
* Install CUDA 11.2, uninstall any other CUDAs
* Get cudnn-11.2-windows-x64-v8.1.1.33.zip and extract it to `C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.2\
* Run build.ps1 (it takes aaaages)
* If build.ps1 crashes with errors about pthread4w:
- Open Developer Command Prompt for VS2017
-  `git clone https://github.com/jwinarske/pthreads4w.git`
-  `cd pthreads4w`
-  `cmake -S . -B build -G "NMake Makefiles" -D BUILD_TESTING=OFF -D CMAKE_BUILD_TYPE=Debug`
-  Copy all of the dll files from e.g. `C:\Users\arron\Code\pthreads4w\build` to e.g. `C:\darknet\build_release\vcpkg_installed\x64-windows\bin`
* Open `"C:\light-surgeons-tracing-the-circle\darknet\build\darknet\darknet.sln"` in vs
* Retargt solution to 2019
* Set the `cudnn` user macro to `C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.2\cuda`
* Put `cudnn64_8.dll` in `C:\light-surgeons-tracing-the-circle\darknet\build\darknet\x64`
* Define `OPENCV` as preprocessor definition.
* Remove `OPENCV` from preprocessor definition undefines
* Hit Build, comment out the bits that cause a fuss
* Get OCV 4.5.3 from the internet. Unzip it.
* Put `opencv_world453.lib` (and also its debug counterpart) in i`C:\light-surgeons-tracing-the-circle\darknet\vcpkg\packages\opencv4_x64-windows\x64\vc15\lib`
* Put `opencvworld` dlls and pdbs in `C:\light-surgeons-tracing-the-circle\darknet\build\darknet\x64`
* Remove cudnn from preprocessor directives.

.\darknet detector demo cfg/combine9k.data cfg/yolo9000.cfg "C:\light-surgeons-tracing-the-circle\darknet\weights\yolo9000.weights" -c 0
