const fs = require('fs');
const path = require('path');
const express = require('express');
const app =  express();
const httpClient = require('http');
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const nconf = require('nconf');
const winston = require('winston');
const osc = require('osc');
const {spawn} = require('child_process');
const glob = require('glob');
const exec = require('child_process').exec;

/*Config*/
//If config_local doesn't exist then create it by copying config_defaults, then tell nconf to use it:
const localConfigFile = 'config_local.json';
if (!fs.existsSync(localConfigFile)) {
	let defaultConfig = require('./config_defaults.json');
	fs.writeFileSync(localConfigFile, JSON.stringify(defaultConfig, null, 2));
}

nconf.argv().env().file({file: localConfigFile});

/*Logging*/
const logFormat = winston.format.printf(({ level, message, label, timestamp }) => {
	return `${timestamp} [${label}] ${level}: ${message}`;
});

const launchTimeString = new Date().toISOString().replace('T', '_').replace(/[Z\-:\.]/g, '').slice(0, -3);
const logger = winston.createLogger({
	level: 'info',
	format: winston.format.combine(
		winston.format.label({label: 'MiddleBit'}),
		winston.format.timestamp(),
		logFormat
	),
	transports: [
		new winston.transports.Console({
			format: winston.format.combine(
				winston.format.colorize(),
				winston.format.label({label: 'MiddleBit'}),
				winston.format.timestamp(),
				logFormat
			),
		}),
		new winston.transports.File({ filename: __dirname + '/logs/MiddleBit_Combined_' + launchTimeString + '.log' , level: 'info'})
	],
	exceptionHandlers: [
		new winston.transports.Console(),
		new winston.transports.File({ filename: __dirname + '/logs/MiddleBit_Exceptions_' + launchTimeString + '.log' })
	]
});
logger.info('MiddleBit started.');

/* Processes */
function spawnProcess(processPath, args, cwd, onSuccess, onFail) {
    logger.info("Spawning process " + processPath + " " + args.join(' '));

    let p = spawn(processPath, args, {cwd : cwd, shell: true});

    p.on("error", function(err) {
        logger.error("Couldn't spawn " + processPath + " with args: " + args.join(' '));
        onFail();
    });

    p.on("exit", function(code, signal) {
        if (code === 0) {
            logger.info( "Process exited with code " + code);
            onSuccess();
        } else {
            logger.warn( "Process exited with code " + code);
            onFail();
        }
    });

    p.stdout.on("data", function(data) {
        logger.info("process stdout:" + data.toString().replace(/\r?\n|\r/g, " "));
    });

    p.stderr.on("data", function(data) {
        logger.info("process stderr:" + data.toString().replace(/\r?\n|\r/g, " "));
    });
}

/* FFMPEG */
const ffmpegPath = nconf.get('ffmpegOutput:path');
const ffmpegOutputSetupArgs = nconf.get('ffmpegOutput:setupArgs');
const ffmpegOutputEncodingArgs = nconf.get('ffmpegOutput:encodingArgs');

function ffmpegImageSequenceToPR4444(inputPath, outputPath, onSuccess, onFail) {
	let ffmpegInputArgs = [];
	ffmpegInputArgs.push("-i");
	ffmpegInputArgs.push(inputPath);

	let ffmpegArgs = ffmpegOutputSetupArgs.concat(ffmpegInputArgs).concat(ffmpegOutputEncodingArgs);
	ffmpegArgs.push(outputPath);
	logger.info("Spawning FFMPEG with args " + ffmpegArgs);

	var ffmpeg = spawn(ffmpegPath, ffmpegArgs);

	ffmpeg.on("error", function(err) {
		logger.error("Error spawning FFMPEG: " + err);
		onFail();
	})

	ffmpeg.on("exit", function(code) {
		if (code === 0) {
			logger.info("FFMPEG complete");
			onSuccess();
		} else {
			logger.error("FFMPEG exited with code " + code);
			onFail();
		}
	});

	ffmpeg.stderr.on('data', function(data){
		logger.info("FFMPEG stderr: " + data.toString().replace(/\r?\n|\r/g, " "));
	})
}

/* Jobs */
const jobStages = {
	NEW : "new",
	RENDERING_YOLACT : "rendering-yolact",
	YOLACT_RENDERING_COMPLETE : "yolact-rendering-complete",
	TRANSCODING_YOLACT_MASKS_MOV : "transcoding-yolo-masks-mov",
	TRANSCODING_YOLACT_MASKS_MOV_COMPLETE : "transcoding-yolo-masks-mov-complete",
	TRANSCODING_YOLACT_BOXES_MOV : "transcoding-yolo-boxes-mov",
	TRANSCODING_YOLACT_BOXES_MOV_COMPLETE : "transcoding-yolo-boxes-mov-complete",
	TRANSCODING_YOLACT_LABELS_MOV : "transcoding-yolo-labels-mov",
	TRANSCODING_YOLACT_LABELS_MOV_COMPLETE : "transcoding-yolo-labels-mov-complete",
	FINISHED : "complete",
	ERROR : "error"
}

const jobs = nconf.get('jobs:queue');
var activeJob = null;

function restoreActiveJobFromDisc() {
	let j = nconf.get('jobs:activeJobPath');
	if (j) {
		activeJob = jobs.find(jb => jb.inputVideoPath === j);
		logger.info("Loaded job " + activeJob.inputVideoPath + " from disc.");
	}
}
restoreActiveJobFromDisc();

function clearActiveJob() {
	activeJob = null;
	nconf.set('jobs:activeJobPath', null);
	nconf.save();
	logger.info("Cleared active job.");
}

function setJobState(job, state) {
	job.currentState = state;
	logger.info("Changed job " + job.inputVideoPath + " state to " + state);
}

function makeNewJob(inputVideoPath) {
	let job = {};
	job.inputVideoPath = inputVideoPath;

	let clipBaseName = path.basename(inputVideoPath);
	let lastDotIndex = clipBaseName.lastIndexOf('.');
	job.clipName = clipBaseName.slice(0, lastDotIndex);
	job.currentState = jobStages.NEW;
	job.created = new Date().toString();
	job.completed = null;

	jobs.push(job);
	nconf.save();

	logger.info("Created new job:\n" + JSON.stringify(job));
}

function findNewJobs() {
	let inputPath = nconf.get('pipeline:inputDirectory');
	//logger.info("Looking for new videos in " + inputPath);

	let inputVideos = glob.sync(inputPath + '/**/*.mp4', {cwd: inputPath});
	inputVideos.forEach(function(v){
		let rel = path.relative(inputPath, v);
		let matches = jobs.filter(j => j.inputVideoPath === rel);
		if (matches.length === 0) {
			makeNewJob(rel);
		}
	});
}

function checkForRequiredActions() {
	let inputPath = nconf.get('pipeline:inputDirectory');
	let tempPath = nconf.get('pipeline:workingDirectory');

	findNewJobs();

	if (activeJob === null) {
		let j = jobs.find(j => j.currentState !== jobStages.FINISHED && j.currentState !== jobStages.ERROR);
		if (j) {
			activeJob = j;
			nconf.set('jobs:activeJobPath', activeJob.inputVideoPath);
			nconf.save();
		} else {
			return;
		}
	}

	if (activeJob.currentState === jobStages.NEW) {
		let imageSequencePath = path.join(tempPath, path.dirname(activeJob.inputVideoPath), activeJob.clipName, "out");
		fs.mkdirSync(imageSequencePath, {recursive: true});
		logger.info("Created " + imageSequencePath);

		//Spawn yolact:
		let args = JSON.parse(JSON.stringify(nconf.get('yolact:args')));
		args.push('--video="' + path.join(inputPath, activeJob.inputVideoPath) + '":"' + path.join(imageSequencePath, 'dummy.mp4"'));

		logger.info("Spawning yolact with args " + args.join(" "));
		spawnProcess(nconf.get('yolact:path'), args, __dirname,
			function() {
				logger.info("Yolact finished.");
				setJobState(activeJob, jobStages.YOLACT_RENDERING_COMPLETE);
				nconf.save();
			},
			function(err){
				setJobState(activeJob, jobStages.ERROR);
				nconf.save();
			});


		setJobState(activeJob, jobStages.RENDERING_YOLACT);
	}

	if (activeJob.currentState === jobStages.YOLACT_RENDERING_COMPLETE) {
		activeJob.currentState = jobStages.TRANSCODING_YOLACT_MASKS_MOV;

		let inputPath = path.join(tempPath, path.dirname(activeJob.inputVideoPath), activeJob.clipName, "out", "edges_%7d.png");
		logger.info("Transcoding from " + inputPath);
		let outputPath = path.join(nconf.get('pipeline:outputDirectory'), path.dirname(activeJob.inputVideoPath), activeJob.clipName + " YOLACT MASKS.mov");
		fs.mkdirSync(path.dirname(outputPath), {recursive: true});

		ffmpegImageSequenceToPR4444(inputPath, outputPath,
			function() {
				setJobState(activeJob, jobStages.TRANSCODING_YOLACT_MASKS_MOV_COMPLETE);
				nconf.save();
			},
			function () {
				setJobState(activeJob, jobStages.ERROR);
			});
	}

	if (activeJob.currentState === jobStages.TRANSCODING_YOLACT_MASKS_MOV_COMPLETE) {
		activeJob.currentState = jobStages.TRANSCODING_YOLACT_BOXES_MOV;

		let inputPath = path.join(tempPath, path.dirname(activeJob.inputVideoPath), activeJob.clipName, "out", "boxes_%7d.png");
		let outputPath = path.join(nconf.get('pipeline:outputDirectory'), path.dirname(activeJob.inputVideoPath), activeJob.clipName + " YOLACT BOXES.mov");
		ffmpegImageSequenceToPR4444(inputPath, outputPath,
			function() {
				setJobState(activeJob, jobStages.TRANSCODING_YOLACT_BOXES_MOV_COMPLETE);
				activeJob = null;

				nconf.save();
			},
			function () {
				setJobState(activeJob, jobStages.ERROR);
			});
	}

	if (activeJob.currentState === jobStages.TRANSCODING_YOLACT_BOXES_MOV_COMPLETE) {
		activeJob.currentState = jobStages.TRANSCODING_YOLACT_LABELS_MOV;

	let inputPath = path.join(tempPath, path.dirname(activeJob.inputVideoPath), activeJob.clipName, "out", "labels_%7d.png");
	let outputPath = path.join(nconf.get('pipeline:outputDirectory'), path.dirname(activeJob.inputVideoPath), activeJob.clipName + " YOLACT LABELS.mov");
	ffmpegImageSequenceToPR4444(inputPath, outputPath,
		function() {
			setJobState(activeJob, jobStages.TRANSCODING_YOLACT_LABELS_MOV_COMPLETE);
			activeJob = null;

			nconf.save();
		},
		function () {
			setJobState(activeJob, jobStages.ERROR);
		});
}

if (activeJob.currentState === jobStages.TRANSCODING_YOLACT_LABELS_MOV_COMPLETE) {
	logger.info("Job complete for " + activeJob.inputVideoPath + ". Deleting temp files");

	let dir = path.join(tempPath, path.dirname(activeJob.inputVideoPath));

	try {
		fs.rmdirSync(dir, {recursive: true});
		setJobState(activeJob, jobStages.FINISHED);
		nconf.save();
	} catch (ex) {
		logger.error("Couldn't delete temp directory " + dir + " :" + ex);
		setJobState(activeJob, jobStages.ERROR);
		nconf.save();
	}

	clearActiveJob();

	logger.info("Rebooting to clear GPU RAM.");
	exec('sudo reboot');
}


if (activeJob && activeJob.currentState === jobStages.ERROR) {
	logger.error("Unloading job " + activeJob.inputVideoPath + " because it is in error state.");
	clearActiveJob();
}

io.emit('/currentJob', activeJob);
io.emit('/jobs', jobs)
}

setInterval(checkForRequiredActions, 1000);

/*Webserver*/
app.get('/', function (req, res) {
	res.sendFile(__dirname + '/index.html');
});

app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use('/styles.css', express.static(__dirname + '/styles.css'));

/*WS*/
io.on('connection', function (socket) {
	logger.info("New socketio connection from " + socket.handshake.address);

});

http.listen(nconf.get('server:httpListenPort'), function () {
	logger.info('listening on *:' + nconf.get('server:httpListenPort'));
});

